# == Class ckan::install
#
# installs ckan
# details: http://docs.ckan.org/en/ckan-2.0/install-from-package.html
#
class ckan::install {

  anchor{'ckan::install::begin':}

  ## Variables

  # determine ckan
  if $ckan::ckan_version == undef {
    # report error
    fail('ckan_version not set!')
  }

  # use ckan.org's package repository if not specified
  if $ckan::ckan_package_url == undef and
    $ckan::ckan_package_filename == undef {
    $ckan_package_filename = "python-ckan_${ckan::ckan_version}_amd64.deb"
    $ckan_package_url = "http://packaging.ckan.org/${ckan_package_filename}"
  }else {
    $ckan_package_filename = $ckan::ckan_package_filename
    $ckan_package_url = $ckan::ckan_package_url
  }

  # determine solr
  if $ckan::solr_schema_version == 'default' {
    $solr_schema_path = "${ckan::ckan_src}/ckan/config/solr/schema.xml"
  }else {
    $solr_schema_path = "${ckan::ckan_src}/ckan/config/solr/\
schema-${ckan::solr_schema_version}.xml"
  }

  class {'wget':
    require => Anchor['ckan::install::begin'],
  }

  # Install Postgres
  class { 'postgresql::server':
    pg_hba_conf_defaults => $ckan::pg_hba_conf_defaults,
    postgres_password    => $ckan::postgres_pass,
    listen_addresses     => '*',
    require              => Class['wget'],
  }

  # Install CKAN deps
  package { $ckan::required_packages:
    ensure  => installed,
    require => Class['postgresql::server'],
  }

  #TODO ensure this only runs once!
  exec { 'a2enmod wsgi':
    command => $ckan::wsgi_command,
    creates => $ckan::wsgi_command,
    require => Package[$ckan::required_packages],
  }

  # Install CKAN either from APT repositories or from the specified file
  if $ckan::is_ckan_from_repo == true {
    package { 'python-ckan':
      ensure  => latest,
      #require => Package[$ckan::required_packages],
      require => Exec ['a2enmod wsgi'],
      before  => Class['nodejs'],
    }
  } else {
    file { $ckan::ckan_package_dir:
      ensure  => directory,
      require => Exec ['a2enmod wsgi'],
    }
    wget::fetch { 'ckan package':
      source      => $ckan_package_url,
      destination => "${ckan::ckan_package_dir}/${ckan_package_filename}",
      verbose     => false,
      require     => File[$ckan::ckan_package_dir],
    }
    package { 'python-ckan':
      ensure   => latest,
      provider => dpkg,
      source   => "${ckan::ckan_package_dir}/${ckan_package_filename}",
      require  => Wget::Fetch['ckan package'],
      before   => Class['nodejs'],
    }
  }


  # Install Node and NPM (which comes with Node)
  class{'nodejs':
    require => Exec ['a2enmod wsgi'],
  }

  # less requires a compile of the css before changes take effect.
  exec { 'Install Less and Nodewatch':
    command => '/usr/bin/npm install less nodewatch',
    cwd     => '/usr/lib/ckan/default/src/ckan',
    #require => [Package['nodejs','python-ckan']],
    require => Class['nodejs'],
    creates => '/usr/lib/ckan/default/src/ckan/bin/less',
  }

  # Git is necessary for installing extensions
  package { 'git-core':
    ensure  => present,
    require => Exec [ 'Install Less and Nodewatch'],
  }

  # setup the plugin collector
  file {'/opt/ckan_plugin_collector':
    ensure  => directory,
    require => Package['git-core'],
  }

  file {'/opt/ckan_plugin_collector/plugin_collector.bash':
    ensure  => file,
    source  => 'puppet:///modules/ckan/plugin_collector.bash',
    mode    => '0755',
    require => File['/opt/ckan_plugin_collector'],
  }

  # Install Solr/Jetty
  class{'solr':
    url     => $ckan::solr_url,
    version => $ckan::solr_version,
    timeout => 0, # disable timeout
    require => File ['/opt/ckan_plugin_collector/plugin_collector.bash'],
  }

  # Jetty configuration
  # Note, internally to core, it notifies class solor so the require is
  # not necessary.
  solr::core{'ckan':
    schema_src_file => $solr_schema_path,
    require         => Class['solr'],
  }

  anchor{'ckan::install::end':
    require => Solr::Core['ckan'],
  }
}
