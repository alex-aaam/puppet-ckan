# == Class ckan::postinstall
#
# Manages services for ckan
#
# details: http://docs.ckan.org/en/ckan-2.0/install-from-package.html
class ckan::service {
  
  anchor{'ckan::service::begin':}

  # update the plugin configuration
  # must run every run
  exec {'update_plugins':
    command => "/opt/ckan_plugin_collector/plugin_collector.bash\
 /etc/ckan/plugins",
    #notify  => Class['solr'],
    require => Anchor['ckan::service::begin'],
  }
  exec {'restart jetty':
    command     => '/usr/sbin/service jetty restart',
    refreshonly => true,
    require     => Exec['update_plugins'],
  }

  service { $ckan::apache_service:
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
    require    => Exec['restart jetty'],
  }
  service { 'nginx':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
    require    => Service[$ckan::apache_service],
  }
  anchor{'ckan::service::end':
    require => Service['nginx'],
  }
}
