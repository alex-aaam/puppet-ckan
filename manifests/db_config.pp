# configuration supporting database for ckan
# details: http://docs.ckan.org/en/ckan-2.0/install-from-package.html
#
# ## Variables
#
# [*password_hash*]
#   The password to use for accessing the database.
#
class ckan::db_config {

  anchor{'ckan::db_config::begin':}

  $password_hash = postgresql_password('ckan_default', $ckan::ckan_pass)
  $datastore_password_hash =
  postgresql_password('datastore_default', $ckan::ckan_pass)

  # === configure postgresql ===
  # create the database
  # also create the user/password to access the db
  # user gets all privs by default
  postgresql::server::role {'ckan_default':
    password_hash => $password_hash,
    require       => Anchor['ckan::db_config::begin'],
  }
  postgresql::server::db {'ckan_default':
    user     => 'ckan_default',
    owner    => 'ckan_default',
    password => $password_hash,
    require  => Postgresql::Server::Role ['ckan_default'],
  }
  # create a seperate db for the datastore extension
  postgresql::server::db { 'datastore_default' :
    user     => 'ckan_default',
    owner    => 'ckan_default',
    password => $password_hash,
    require  => Postgresql::Server::Role ['ckan_default'],
  }
  # create a ro user for datastore extension
  postgresql::server::role { 'datastore_default' :
    password_hash => $datastore_password_hash,
    require       => Postgresql::Server::Db [ 'datastore_default' ],
  }
  # grant privs for datastore user
  postgresql::server::database_grant { 'datastore_default' :
    privilege => 'CONNECT',
    db        => 'datastore_default',
    role      => 'datastore_default',
    require   => Postgresql::Server::Role['datastore_default'],
  }
  anchor{'ckan::db_config::end':
    require => Postgresql::Server::Database_grant [ 'datastore_default' ],
  }
}
