# == Class: ckan::ext::repeating
#
# Installs the "repeating" extension.
#
# This extension provides a way to store repeating fields in CKAN datasets,
# resources, organizations and groups.
#
class ckan::ext::repeating {
  ckan::ext { 'repeating':
    source   => 'git://github.com/open-data/ckanext-repeating.git',
    revision => 'master',
    plugin   => 'repeating',
  }
}
