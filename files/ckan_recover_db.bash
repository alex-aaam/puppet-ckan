#!/bin/bash
# recovers the db from the default location
. /usr/lib/ckan/default/bin/activate
cd /usr/lib/ckan/default/src/ckan
paster db clean --config=/etc/ckan/default/production.ini /backup/ckan_database.pg_dump
paster db load --config=/etc/ckan/default/production.ini /backup/ckan_database.pg_dump